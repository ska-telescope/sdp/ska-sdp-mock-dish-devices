from dataclasses import dataclass

from astropy.time import Time


@dataclass
class TimestampInfo:
    count: int
    """Number of timestamps in a scan"""
    first: Time
    """First timestamp in a scan"""
    last: Time
    """Last timestamp in a scan"""
    epoch: Time = Time("2000-01-01", format="isot", scale="tai")
    """
    Epoch time used by raw timestamp values. Default is 1999-12-31T23:59:28Z UTC.
    """


@dataclass
class ObservationData:
    achieved_pointings: dict[int, str]
    """Scan number to achieved pointing dataset"""
    expected_pointings: dict[int, str]
    """Scan number to expected pointing dataset"""
    source_offsets: dict[int, str]
    """Scan number to source offset dataset"""
    timestamp_info: dict[int, TimestampInfo]
    """Scan number to expected timestamp info"""


OBSERVATIONS = {
    "MID-AA0.5": ObservationData(
        achieved_pointings={
            1: "tests/data/MID-AA0.5/pointing_actual_scan_1.hdf.tar.gz",
            2: "tests/data/MID-AA0.5/pointing_actual_scan_2.hdf.tar.gz",
        },
        expected_pointings={
            1: "tests/data/MID-AA0.5/pointing_nominal_scan_1.hdf.tar.gz",
            2: "tests/data/MID-AA0.5/pointing_nominal_scan_2.hdf.tar.gz",
        },
        source_offsets={},
        timestamp_info={
            1: TimestampInfo(
                count=240,
                first=Time("2000-01-01T10:34:12.769956589Z", scale="utc"),
                last=Time("2000-01-01T11:04:05.269956589Z", scale="utc"),
            ),
            2: TimestampInfo(
                count=240,
                first=Time("2000-01-01T12:01:55.272452354Z", scale="utc"),
                last=Time("2000-01-01T12:31:47.772452354Z", scale="utc"),
            ),
        },
    ),
    "MID-AA0.5-merged": ObservationData(
        achieved_pointings={
            1: "tests/data/MID-AA0.5/pointing_actual_scan_1_merged.hdf.tar.gz",
            2: "tests/data/MID-AA0.5/pointing_actual_scan_2_merged.hdf.tar.gz",
        },
        expected_pointings={
            1: "tests/data/MID-AA0.5/pointing_nominal_scan_1_merged.hdf.tar.gz",
            2: "tests/data/MID-AA0.5/pointing_nominal_scan_2_merged.hdf.tar.gz",
        },
        source_offsets={},
        timestamp_info={
            1: TimestampInfo(
                count=240,
                first=Time("2000-01-01T10:34:12.769956589Z", scale="utc"),
                last=Time("2000-01-01T11:04:05.269956589Z", scale="utc"),
            ),
            2: TimestampInfo(
                count=240,
                first=Time("2000-01-01T12:01:55.272452354Z", scale="utc"),
                last=Time("2000-01-01T12:31:47.772452354Z", scale="utc"),
            ),
        },
    ),
    "MeerKAT": ObservationData(
        achieved_pointings={
            1: "tests/data/MeerKAT/actual_pointing_scan_1.hdf.tar.gz",
            2: "tests/data/MeerKAT/actual_pointing_scan_2.hdf.tar.gz",
        },
        expected_pointings={
            1: "tests/data/MeerKAT/requested_pointing_scan_1.hdf.tar.gz",
            2: "tests/data/MeerKAT/requested_pointing_scan_2.hdf.tar.gz",
        },
        source_offsets={
            1: "tests/data/MeerKAT/source_offset_scan_1.hdf.tar.gz",
            2: "tests/data/MeerKAT/source_offset_scan_2.hdf.tar.gz",
        },
        timestamp_info={
            1: TimestampInfo(
                count=2,
                first=Time("2023-01-03T06:38:47.627388000Z", scale="utc"),
                last=Time("2023-01-03T06:38:55.624005318Z", scale="utc"),
            ),
            2: TimestampInfo(
                count=2,
                first=Time("2023-01-03T06:39:19.613857269Z", scale="utc"),
                last=Time("2023-01-03T06:39:27.610473633Z", scale="utc"),
            ),
        },
    ),
}

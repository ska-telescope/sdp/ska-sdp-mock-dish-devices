# Test Data Sets

## Observation 1

Scale: AA0.5-Mid
Antennas: 4
Channels: 32
Timesteps: 240
Actual Deviation: 4arcmin
Simulated: Yes

| First Timestamp                |   Ra   |   Dec  | Actual | Nominal |
| ------------------------------ | ------ | ------ | ------ | -------- |
| 2000-01-01T10:34:12.769956589Z | 295.35 | -29.50 | [pointing_actual_scan_1.hdf](https://console.cloud.google.com/storage/browser/_details/ska1-simulation-data/simulations/pointing-offset_SP-3221/orc-1690/global-4arcmin/global_p_ra_295.35_dec_-29.50/SKA_MID-AA0.5_SIM_custom_B2_random_pointing_nchan32_pt_actual.hdf;tab=live_object) <br> pointing_actual_scan_1_merged.hdf | [pointing_nominal_scan_1.hdf](https://console.cloud.google.com/storage/browser/_details/ska1-simulation-data/simulations/pointing-offset_SP-3221/orc-1690/global-4arcmin/global_p_ra_295.35_dec_-29.50/SKA_MID-AA0.5_SIM_custom_B2_random_pointing_nchan32_pt_nominal.hdf;tab=live_object) <br> pointing_nominal_scan_1_merged.hdf
| 2000-01-01T12:01:55.272452354Z | 294.85 | -30.0  | [pointing_actual_scan_2.hdf](https://console.cloud.google.com/storage/browser/_details/ska1-simulation-data/simulations/pointing-offset_SP-3221/orc-1690/global-4arcmin/global_p_ra_294.85_dec_-30.0/SKA_MID-AA0.5_SIM_custom_B2_random_pointing_nchan32_pt_actual.hdf;tab=live_object)<br> pointing_actual_scan_2_merged.hdf | [pointing_nominal_scan_2.hdf](https://console.cloud.google.com/storage/browser/_details/ska1-simulation-data/simulations/pointing-offset_SP-3221/orc-1690/global-4arcmin/global_p_ra_294.85_dec_-30.0/SKA_MID-AA0.5_SIM_custom_B2_random_pointing_nchan32_pt_nominal.hdf;tab=live_object)<br> pointing_nominal_scan_2_merged.hdf

### Note

"*_merged.hdf" files are converted to a single table by importing with [import_pointingtable_from_hdf5](https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/blob/main/src/ska_sdp_datamodels/calibration/calibration_functions.py?ref_type=heads#L354), merging xarrays and exporting with [import_pointingtable_from_hdf5](https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/blob/main/src/ska_sdp_datamodels/calibration/calibration_functions.py?ref_type=heads#L238)

## Observation 2

Scale: MeerKAT AA0.5
Antennas: 4
Channels: 32
Timesteps: 2

| First Timestamp                |   Ra   |   Dec  | Actual | Nominal | Source Offset |
| ------------------------------ | ------ | ------ | ----- | -------- | ------------- |
| 2023-01-03T06:38:47.627388000Z |        |        |[actual_pointing_scan_1.hdf](https://storage.googleapis.com/ska1-simulation-data/simulations/pointing-offset_SP-3221/MeerKAT/interferometric_pointing/no_cal_applied/meerkat_aa0.5/split_into_scans/pointing_hdf/actual_pointing_hdf/actual_pointing_scan_1.hdf)|[requested_pointing_scan_1.hdf](https://storage.googleapis.com/ska1-simulation-data/simulations/pointing-offset_SP-3221/MeerKAT/interferometric_pointing/no_cal_applied/meerkat_aa0.5/split_into_scans/pointing_hdf/requested_pointing_hdf/requested_pointing_scan_1.hdf)| [source_offset_scan_1.hdf](https://storage.googleapis.com/ska1-simulation-data/simulations/pointing-offset_SP-3221/MeerKAT/interferometric_pointing/no_cal_applied/meerkat_aa0.5/split_into_scans/pointing_hdf/source_offset_hdf/source_offset_scan_1.hdf)
| 2023-01-03T06:39:19.613857269Z |        |        |[actual_pointing_scan_2.hdf](https://storage.googleapis.com/ska1-simulation-data/simulations/pointing-offset_SP-3221/MeerKAT/interferometric_pointing/no_cal_applied/meerkat_aa0.5/split_into_scans/pointing_hdf/actual_pointing_hdf/actual_pointing_scan_2.hdf)|[requested_pointing_scan_2.hdf](https://storage.googleapis.com/ska1-simulation-data/simulations/pointing-offset_SP-3221/MeerKAT/interferometric_pointing/no_cal_applied/meerkat_aa0.5/split_into_scans/pointing_hdf/requested_pointing_hdf/requested_pointing_scan_2.hdf)| [source_offset_scan_2.hdf](https://storage.googleapis.com/ska1-simulation-data/simulations/pointing-offset_SP-3221/MeerKAT/interferometric_pointing/no_cal_applied/meerkat_aa0.5/split_into_scans/pointing_hdf/source_offset_hdf/source_offset_scan_2.hdf)


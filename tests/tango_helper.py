import queue
from contextlib import AbstractContextManager
from typing import Callable

from overrides import override


class SubscribeEventContext(AbstractContextManager):
    """
    Context manager for controlling the lifetime of a tango
    event subscription.
    """

    def __init__(self, proxy, attr_name, event_type, callback):
        self.__proxy = proxy
        self.__attr_name = attr_name
        self.__event_type = event_type
        self.__callback = callback

    @override
    def __enter__(self):
        self.__id = self.__proxy.subscribe_event(
            self.__attr_name, self.__event_type, self.__callback
        )
        return self

    @override
    def __exit__(self, exc_type, exc_value, traceback):
        self.__proxy.unsubscribe_event(self.__id)


class SubscribeEventValueContext(SubscribeEventContext):
    """
    Context manager for controlling the lifetime of a tango
    event subscription with a callback that only processes
    event values.
    """

    def __init__(self, proxy, attr_name: str, event_type, callback: Callable):
        def conditional_callback(event):
            if is_valid_event(event):
                callback(event.attr_value.value)

        super().__init__(proxy, attr_name, event_type, conditional_callback)


class SubscribeEventTensorContext(SubscribeEventContext):
    """
    Context manager for controlling the lifetime of a tango
    event subscription with a callback that only processes
    event values.
    """

    def __init__(
        self,
        proxy,
        attr_name: str,
        attr_shape_name: str,
        event_type,
        callback: Callable,
    ):
        def conditional_callback(event):
            if is_valid_event(event):
                callback(
                    event.attr_value.value.reshape(proxy.read_attribute(attr_shape_name).value)
                )

        super().__init__(proxy, attr_name, event_type, conditional_callback)


def is_valid_event(event):
    return event and event.attr_value and event.attr_value.value is not None


class QueueIterator:
    """Utility for iterating a regular Queue"""

    def __init__(self, source_queue: queue.Queue, timeout: float | None = 0.0):
        self.source_queue = source_queue
        self.timeout = timeout

    def __iter__(self):
        while True:
            try:
                yield self.source_queue.get(timeout=self.timeout)
            except queue.Empty:
                break

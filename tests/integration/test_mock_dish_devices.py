import functools
import itertools
import json
import queue
from contextlib import ExitStack

import numpy as np
import pytest
from astropy.time import TimeDelta
from tango import EventType
from tango.test_context import MultiDeviceTestContext

from ska_sdp_mock_dish_devices.mock_dish_leafnode import MockDishLeafnode
from ska_sdp_mock_dish_devices.mock_dish_master import MockDishMaster
from tests.data.observations import OBSERVATIONS, ObservationData, TimestampInfo
from tests.tango_helper import QueueIterator, SubscribeEventValueContext
from tests.test_utils import untar


@pytest.fixture(name="tango_context")
def tango_context_fixture(num_dishes: int, observation_name: str):
    observation: ObservationData = OBSERVATIONS[observation_name]
    pointings_actual = {}
    for scan_id, actual in observation.achieved_pointings.items():
        pointings_actual.update({scan_id: untar(actual).as_posix()})
    pointings_nominal = {}
    for scan_id, nominal in observation.expected_pointings.items():
        pointings_nominal.update({scan_id: untar(nominal).as_posix()})
    source_offsets = {}
    for scan_id, source_offset in observation.source_offsets.items():
        source_offsets.update({scan_id: untar(source_offset).as_posix()})

    time_scale = 1 / 150.0  # 7.5s -> 20Hz
    context = (
        {
            "class": MockDishMaster,
            "devices": [
                {
                    "name": f"test/dish-master/{antenna_id}",
                    "properties": {
                        "mock_achieved_paths": json.dumps(pointings_actual),
                        "antenna_id": antenna_id,
                        "time_scale": time_scale,
                    },
                }
                for antenna_id in range(num_dishes)
            ],
        },
        {
            "class": MockDishLeafnode,
            "devices": [
                {
                    "name": f"test/dish-leafnode/{antenna_id}",
                    "properties": {
                        "mock_desired_paths": json.dumps(pointings_nominal),
                        "mock_offset_paths": json.dumps(source_offsets),
                        "antenna_id": antenna_id,
                        "time_scale": time_scale,
                    },
                }
                for antenna_id in range(num_dishes)
            ],
        },
    )
    with MultiDeviceTestContext(context, process=False) as context:
        yield context


@pytest.mark.parametrize("observation_name,num_dishes", [("MID-AA0.5", 1)])
def test_mock_dish_master_attributes(tango_context):
    mock_dish = tango_context.get_device("test/dish-master/0")

    np.testing.assert_array_equal(
        mock_dish.achievedPointing,
        np.array([0, 0, 0], dtype="float64"),
        strict=True,
    )


@pytest.mark.parametrize("observation_name,num_dishes", [("MID-AA0.5", 1)])
def test_mock_dish_leafnode_attributes(tango_context):
    mock_dish = tango_context.get_device("test/dish-leafnode/0")

    np.testing.assert_array_equal(
        mock_dish.desiredPointing,
        np.array([0, 0, 0], dtype="float64"),
        strict=True,
    )

    np.testing.assert_array_equal(
        mock_dish.sourceOffset,
        np.array([0, 0, 0], dtype="float64"),
        strict=True,
    )


@pytest.mark.parametrize("num_dishes,scan_id", [(1, 1), (1, 2)])
@pytest.mark.parametrize(
    "observation_name,device_type,attr",
    [
        ("MID-AA0.5", "dish-master", "achievedPointing"),
        ("MID-AA0.5", "dish-leafnode", "desiredPointing"),
        ("MID-AA0.5-merged", "dish-master", "achievedPointing"),
        ("MID-AA0.5-merged", "dish-leafnode", "desiredPointing"),
        ("MeerKAT", "dish-master", "achievedPointing"),
        ("MeerKAT", "dish-leafnode", "desiredPointing"),
        ("MeerKAT", "dish-leafnode", "sourceOffset"),
    ],
)
def test_single_scan_attribute_events(
    tango_context,
    observation_name: str,
    device_type: str,
    attr: str,
    num_dishes: int,
    scan_id: int,
):
    observation = OBSERVATIONS[observation_name]
    timeout = 2.0  # seconds
    event_queue = queue.Queue()

    proxies = [tango_context.get_device(f"test/{device_type}/{id}") for id in range(num_dishes)]

    events = []
    num_events_expected = num_dishes * (observation.timestamp_info[scan_id].count + 1)

    with ExitStack() as stack:
        for proxy in proxies:
            stack.enter_context(
                SubscribeEventValueContext(
                    proxy,
                    attr,
                    EventType.CHANGE_EVENT,
                    event_queue.put,
                )
            )

        for proxy in proxies:
            proxy.Scan(scan_id)

        for event in itertools.islice(
            QueueIterator(event_queue, timeout=timeout), num_events_expected
        ):
            events.append(event)

        for proxy in proxies:
            proxy.EndScan()

    assert len(events) == num_events_expected

    times = np.sort(TimestampInfo.epoch + TimeDelta(np.array(events)[:, 0], format="sec"))

    for antenna_id in range(num_dishes):
        # epoch ts
        # default 0.0 event value represents the epoch
        assert times[antenna_id] == observation.timestamp_info[scan_id].epoch
        # first ts
        assert (
            times[num_dishes + antenna_id].utc.datetime64
            == observation.timestamp_info[scan_id].first.utc.datetime64
        )
        # last ts
        assert (
            times[
                num_dishes * observation.timestamp_info[scan_id].count + antenna_id
            ].utc.datetime64
            == observation.timestamp_info[scan_id].last.utc.datetime64
        )


@pytest.mark.parametrize(
    "observation_name,device_type,attr,num_dishes,scan_ids",
    [
        ("MID-AA0.5", "dish-master", "achievedPointing", 4, [1, 2]),
        ("MID-AA0.5", "dish-leafnode", "desiredPointing", 4, [1, 2]),
        ("MeerKAT", "dish-master", "achievedPointing", 4, [1, 2]),
        ("MeerKAT", "dish-leafnode", "desiredPointing", 4, [1, 2]),
        ("MeerKAT", "dish-leafnode", "sourceOffset", 4, [1, 2]),
    ],
)
def test_multiple_scan_attribute_events(
    tango_context,
    observation_name: str,
    device_type: str,
    attr: str,
    num_dishes: int,
    scan_ids: list,
):
    observation = OBSERVATIONS[observation_name]
    timeout = 5.0  # seconds
    event_queue = queue.Queue()

    proxies = [tango_context.get_device(f"test/{device_type}/{id}") for id in range(num_dishes)]

    events = []

    with ExitStack() as stack:
        for proxy in proxies:
            stack.enter_context(
                SubscribeEventValueContext(
                    proxy,
                    attr,
                    EventType.CHANGE_EVENT,
                    event_queue.put,
                )
            )

        for scan_id in scan_ids:
            for proxy in proxies:
                proxy.Scan(scan_id)

            for event in itertools.islice(
                QueueIterator(event_queue, timeout=timeout),
                num_dishes * (observation.timestamp_info[scan_id].count + 1),
            ):
                events.append(event)

            for proxy in proxies:
                proxy.EndScan()

    obs_timesteps = functools.reduce(
        lambda a, b: a.count + b.count, list(observation.timestamp_info.values())
    )
    assert len(events) == num_dishes * (obs_timesteps + 1)

Mock Dish Leafnode Device
=========================

Introduction
------------

Mock implementation of the SKA TMC Dish Leafnode Tango Device provided by https://gitlab.com/ska-telescope/ska-tmc/ska-tmc-dishleafnode.

Usage
-----

This mock implementation when in the `ON` state will simulate mocked attributes listed below of the real device using a mock dataset. Mocking must be started using the `Start` command and may be stopped (for restart) using the `Stop` command.

Additionally, the device will automatically transition to the `Off` state when at the end of the mock data stream.

For API usage see :class:`ska_sdp_mock_dish_devices.MockDishLeafnode`.

States
------

===== ===========
State Description
===== ===========
OFF   Device is initialized and is not simulating 
----- -----------
ON    Attributes are being continuously simulated from mock data by the device
===== ===========

Properties
----------

================== ============ =================================== ===================================
Property           Type         Values                              Description                        
================== ============ =================================== ===================================
mock_desired_paths String       ''{"1": "mnt/data/pointings.hdf"}'` Json mapping of scan id to a path `PointingTable`_ collections in `HDF5`_ format for `desiredPointing`
------------------ ------------ ----------------------------------- -----------------------------------
mock_offset_paths  String       ''{"1": "mnt/data/pointings.hdf"}'` Json mapping of scan id to a path of `PointingTable`_ collections in `HDF5`_ format for `sourceOffset`
------------------ ------------ ----------------------------------- -----------------------------------
antenna_id         Int32        0                                   Antenna ID within the pointing data
------------------ ------------ ----------------------------------- -----------------------------------
time_scale         Float        1.0                                 Scale factor for the timing interval pointings are read
================== ============ =================================== ===================================

.. _PointingTable: https://developer.skao.int/projects/ska-sdp-datamodels/en/latest/api/ska_sdp_datamodels.calibration.PointingTable.html#pointingtable
.. _HDF5: https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/blob/main/src/ska_sdp_datamodels/calibration/calibration_functions.py#L205

Commands
--------

======= ============= =========== =======================
Command Argument type Return type Action
======= ============= =========== =======================
Scan    int           None        Set device state to ON
------- ------------- ----------- -----------------------
EndScan None          None        Set device state to OFF
======= ============= =========== =======================

Attributes
----------

========================== ======= =========== =================================== ===================================
Attribute                  Type    Format      Values                              Description                        
========================== ======= =========== =================================== ===================================
desiredPointing            Float64 SPECTRUM[3] [:ref:`offset_since_epoch`, az, el] Marks the dish commanded pointing direction in radians for a corresponding timestamp.
========================== ======= =========== =================================== ===================================

.. note::
    For a full list of planned attributes see https://gitlab.com/ska-telescope/ska-sim-dishmaster/-/blob/master/src/ska_sim_dishmaster/dish_master.fgo

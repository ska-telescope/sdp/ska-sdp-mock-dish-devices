SKA SDP Mock Dish Devices
=========================

This project defines a set of Docker images and Docker compose files
that are useful for TANGO control system development.

.. toctree::
   :maxdepth: 2
   :caption: Tango Devices

   mockdishmaster
   mockdishleafnode

.. toctree::
   :maxdepth: 2
   :caption: Guides

   api
   README
   CHANGELOG

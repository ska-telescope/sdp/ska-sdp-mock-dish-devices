Mock Dish Master Device
=======================

Introduction
------------

Mock implementation of the SKA Dish Master Tango device based on the interface provided by https://gitlab.com/ska-telescope/ska-sim-dishmaster.

Usage
-----

This mock implementation when in the `ON` state will simulate mocked attributes listed below of the real device using a mock dataset. Mocking must be started using the `Start` command and may be stopped (for restart) using the `Stop` command.

Additionally, the device will automatically transition to the `Off` state when at the end of the mock data stream.

For API usage see :class:`ska_sdp_mock_dish_devices.MockDishMaster`.

States
------

===== ===========
State Description
===== ===========
OFF   Device is initialized and is not simulating 
----- -----------
ON    Attributes are being continuously simulated from mock data by the device
===== ===========

Properties
----------

=================== ============ =================================== ===================================
Property            Type         Values                              Description                        
=================== ============ =================================== ===================================
mock_achieved_paths String       ''{"1": "mnt/data/pointings.hdf"}'` Json mapping of scan id to a path of `PointingTable`_ collections in `HDF5`_ format for `achievedPointing`
------------------- ------------ ----------------------------------- -----------------------------------
antenna_id          Int32        0                                   Antenna ID within the pointing data
------------------- ------------ ----------------------------------- -----------------------------------
time_scale          Float        1.0                                 Scale factor for the timing interval pointings are read
=================== ============ =================================== ===================================

.. _PointingTable: https://developer.skao.int/projects/ska-sdp-datamodels/en/latest/api/ska_sdp_datamodels.calibration.PointingTable.html#pointingtable
.. _HDF5: https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels/-/blob/main/src/ska_sdp_datamodels/calibration/calibration_functions.py#L205

Commands
--------

======= ============= =========== =======================
Command Argument type Return type Action
======= ============= =========== =======================
Scan    int           None        Set device state to ON
------- ------------- ----------- -----------------------
EndScan None          None        Set device state to OFF
======= ============= =========== =======================

Attributes
----------

========================== ======= =========== =================================== ===================================
Attribute                  Type    Format      Values                              Description                        
========================== ======= =========== =================================== ===================================
achievedPointing           Float64 SPECTRUM[3] [:ref:`offset_since_epoch`, az, el] Marks the dish actual pointing direction in radians for a corresponding timestamp.
========================== ======= =========== =================================== ===================================

.. note::
    For a full list of planned attributes see https://gitlab.com/ska-telescope/ska-sim-dishmaster/-/blob/master/src/ska_sim_dishmaster/dish_master.fgo

.. _offset_since_epoch:

Offset since Epoch
^^^^^^^^^^^^^^^^^^

These are specified as an offset in SI seconds (or multiples/sub-multiples of SI seconds) from the TAI epoch of midnight, 1 January 2000, which equates to 1999-12-31T23:59:28Z UTC. See https://confluence.skatelescope.org/display/SWSI/ADR-78+SKA+approach+to+timestamps+in+time+sensitive+data for more information.

.. note::
    The UTC equivalent is different because at the start of Y2000 UTC was 32 seconds behind TAI (it is now further behind because of leap second adjustments).

import logging
import time
from threading import Event
from typing import Generator

import h5py
import numpy as np
from astropy.time import Time

_MJD_TO_Y2000TAI_SEC: float = (
    Time("2000-01-01", format="isot", scale="tai") - Time(0.0, format="mjd")
).sec


def assert_hdf5_pointing_table(datafile: h5py.File, antenna_id: int):
    """
    Verifies that the HDF5 pointing file contains compatible pointing data.

    For full data model see:
    https://gitlab.com/ska-telescope/sdp/ska-sdp-datamodels
    """
    multiple_frequencies, multiple_receptors = False, False
    has_multiple_tables = len(datafile.keys()) > 1

    for key in datafile.keys():
        group: h5py.Group = datafile[key]  # type: ignore
        assert isinstance(group, h5py.Group)
        assert group.attrs["data_model"] == "PointingTable", "Not a PointingTable"
        assert "data_time" in group.keys()
        assert "data_pointing" in group.keys()

        pointing_shape = group["data_pointing"].shape
        assert len(pointing_shape) == 5  # time, antenna, channel, receptor, angle
        if has_multiple_tables:
            assert pointing_shape[0] == 1
        assert pointing_shape[1] > antenna_id
        multiple_frequencies |= pointing_shape[2] > 1
        multiple_receptors |= pointing_shape[3] > 1
        assert pointing_shape[4] == 2

    if multiple_frequencies:
        logging.warning("pointing data contains multiple frequencies. Averaging will be applied.")
    if multiple_receptors:
        logging.warning("pointing data contains multiple receptors. Averaging will be applied.")


def stream_hdf5_pointings(file: h5py.File, antenna_id: int) -> Generator[np.ndarray, None, None]:
    """Creates a generator to time ordered pointing values read from HDF5 file validated by
    `assert_hdf5_pointing_table`.

    Args:
        file (h5py.File): HDF file of pointing table(s).
        antenna_id (int): antenna_id index within HDF file.

    Yields:
        Generator[np.ndarray, None, None]: pointing generator of [time(s), az, el] ndarrays.
    """
    # sort group keys by timestamp if using multiple tables
    keys = list(file.keys())
    keys.sort(key=lambda k: file[k]["data_time"][0])  # type: ignore
    for key in keys:
        group = file[key]
        for time_idx, data_time_mjds in enumerate(group["data_time"]):
            # coords: time, antenna, frequency, receptor, angle
            pointing_ds: h5py.Dataset = group["data_pointing"]  # type: ignore
            azel: np.ndarray = pointing_ds[time_idx, antenna_id, :, :, :]
            azel_ave = np.average(azel, axis=(0, 1))
            desired_pointing = np.array(
                [
                    data_time_mjds - _MJD_TO_Y2000TAI_SEC,
                    azel_ave[0],
                    azel_ave[1],
                ]
            )
            yield desired_pointing


def time_delay_pointings(
    pointings: Generator[np.ndarray, None, None],
    time_scale: float,
    stop_event: Event,
) -> Generator[np.ndarray, None, None]:
    """Adapts a stream of pointings by applying time delays calculated
    by reinterpreting the first pointing as the current time.

    Args:
        pointings (Generator[np.ndarray, None, None]): pointing generator
        of [time(s), az, el] ndarrays.
        time_scale (float): scale factor applied to time delays.
        stop_event (Event): event to immediately end the generator.

    Yields:
        Generator[np.ndarray, None, None]: spaced-out generator of pointings.
    """
    first_pointing = next(pointings)
    data_start_time = first_pointing[0]
    actual_start_time = time.time()
    yield first_pointing

    for desired_pointing in pointings:
        processing_time = time.time() - actual_start_time
        sleep_time = (desired_pointing[0] - data_start_time) * time_scale - processing_time
        if stop_event.wait(sleep_time):
            break
        if sleep_time < 0:
            logging.warning(
                "mock dish device not keeping up, behind %ss",
                -sleep_time,
            )
        yield desired_pointing

###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[Development]
*************

[0.5.1]
*******

Fixed
-----

* Python logging setup now correctly follows
  the ``-v`` command line option.

[0.5.0]
*******

Changed
-------

* **BREAKING** Moved `sourceOffset` and `mock_offset_paths` to Mock Dish Leafnode

[0.4.0]
*******

Added
-----

* Added support for time axis in pointing table
* Added `sourceOffset` attribute to Mock Dish Master
* Added `mock_offset_paths` property for source offset data to Mock Dish Master

Changed
-------

* **BREAKING** Renamed `mock_data_paths` attribute to `mock_achieved_paths` on Mock Dish Master
* **BREAKING** Renamed `mock_data_paths` attribute to `mock_desired_paths` on Mock Dish Leafnode


[0.3.0]
*******

Added
-----

* Added support for multiple scans of datasets.

Changed
-------

* Changed `Start()` command to `Scan(scan_id)`
* Changed `Stop()` command to `EndScan()`
* Changed `mock_data_path` to `mock_data_paths`

[0.2.0]
*******

Added
-----

* Mock Dish Master and Mock Dish Leafnode doc pages

Changed
-------

* Changed `desired_pointing` attribute name to `desiredPointing`
* Changed `achieved_pointing` attribute name to `achievedPointing`

[0.1.0]
*******

Added
-----

* Added Mock Dish Leafnode
* Added Mock Dish Master

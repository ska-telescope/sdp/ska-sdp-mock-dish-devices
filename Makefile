# Include base Makefile functionality
include .make/base.mk
include .make/python.mk
include .make/oci.mk

# As an alternative to specifying process=True on every DeviceTestContext, run
# our tests as a seperate process using --forked which should work every time
PYTHON_VARS_AFTER_PYTEST = --forked
DOCS_SPHINXOPTS = -W --keep-going
PYTHON_LINE_LENGTH = 99

docs-pre-build:
	poetry config virtualenvs.create $(POETRY_CONFIG_VIRTUALENVS_CREATE)
	poetry install --with docs

docs-serve:
	sphinx-autobuild docs/src docs/build/